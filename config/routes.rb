Rails.application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'application#index'
  get "/correo" => "application#email"
  resources :users
  resources :plants
  # conex fb
  get "/auth/:provider/callback" => "users#connect"
  post "/login" => "users#login", as: "login"
  get "/signout" => "users#signout", as: "signout"
  get "/map" => "application#showmap" , as: "map"

  get "/plants" => "users#showlist", as: "list"

  get "/:short" => "application#language"

  #get "verifica/:user", to: "users#email_verification", as: "confirmemail"
  get "verifica/:uuid", to: "users#email_verification", as: "confirmemail"

  get "/users/:id/favorites" => "users#favoritos", as: "favorites"
  get "/users/:id/history" => "users#historial", as: "history"

  patch "/users/:id/inactive" => "users#inactive", as: "inactive"

  patch "/plants/:id/addfavs" => "plants#addfavs", as: "addfavs"

  patch "/users/:id/changerole" => "users#changerole", as: "changerole"

  patch "/users/:id/activacionadmin" => "users#activacionadmin", as: "activacionadmin"
  
  get "forgot/f", to: "users#forgot_pass", as:"forgot_view"
  post "/forgot" => "users#forgot_check", as: "forgot_check"
  
  get "forgot/:uuid" => "users#forgot_change", as: "forgot_change"
  patch "/forgot/:uuid/success" => "users#forgot_success", as: "forgot_success"

  #ruta deb ir    #metodo        #llamad a la vista

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
