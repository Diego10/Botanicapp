# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

lo = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed eros sed elit sodales dictum a vitae leo. Cum sociis natoque penatibus et magnis dis parturient monte'
lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed eros sed elit sodales dictum a vitae leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam porttitor imperdiet nunc, a bibendum massa egestas at. Etiam eu ultricies mi, vitae semper massa. Suspendisse vulputate lacus justo, non venenatis massa tincidunt et. Sed vitae venenatis erat, at interdum massa. Aliquam erat volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse quis libero gravida, luctus augue non, faucibus nisl. Mauris justo massa, efficitur eget felis vehicula, congue vestibulum dui. In scelerisque enim eu erat aliquet, a elementum orci iaculis.'

sha = ' ad61ee8f19f3d7d6f4ae2b44e18f35b3aa6bb8be'
pass = 'america'

User.create(name: 'admin',username: 'admin', email: 'admin@admin.com', gender: 'male', password: pass,verified: true, role: 0, estado: 1 )
User.create(name: 'diego',username: 'diego', email: 'diego@diego.com', gender: 'male', password: pass,verified: true, role: 2, estado: 1 )

Plant.create(name: 'Arbol Salitre 1', common_name: 'AS 1', resume: lo, description: lorem, distribution: 'Cali', etnobotanic: 'Hola', latitude: 4.645830, longitude: -74.057671)
Plant.create(name: 'Arbol Salitre 2', common_name: 'AS 1', resume: lo, description: lorem, distribution: 'Cali', etnobotanic: 'Hola', latitude: 4.657020, longitude: -74.111036)

#:common_name, :resume,:description,:distribution,:etnobotanic