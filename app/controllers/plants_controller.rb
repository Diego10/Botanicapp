class PlantsController < ApplicationController
  def index
    @plants = Plant.all
    @user = User.find(session[:user])
    #render "index"
    respond_to do |format|
      format.html
      format.json {render json: @plants}
    end
    
  end

  def new
    if session[:user]
  	   @user = User.find(session[:user]) 
      render "new_plant"
    end
  end

  def create
    @plant = Plant.new(plant_params)
    @plant.user_id = session[:user]

    if plant_params[:images]

    if plant_params[:images].length.to_i <= 5


      for i in 0...plant_params[:images].length


        if plant_params[:images][i] && !plant_params[:images][i].is_a?(String)
          if @plant.images[i] && Dir.glob("public#{i}_#{@plant.name}").count > 0
            File.delete("public#{i}_#{@plant.name}")
          end

          pushval = "/uploads/plants/" + i.to_s + "_" + @plant.name + "." + plant_params[:images][i].original_filename.split(".").last
          #puts "RUTA: "+ pushval
          @plant.images[i] = pushval
          #puts "RUTA2: "+ @plant.images[i]

          File.open('public' + @plant.images[i], 'wb') do |f|
            f.write(plant_params[:images][i].read)
          end

        end
      end

      if @plant.save
        redirect_to @plant
      else
        render "new_plant"
      end

    else
      @plant.errors.add(:images, "Maximo 5 imgs") if plant_params[:images].length.to_i > 5
      render "new_plant"
    end
  else
      if @plant.save
        redirect_to @plant
      else
        render "new_plant"
      end
  end

  end

  def show
    @user = User.find(session[:user]) if session[:user]
    @plant = Plant.find(params[:id])
    respond_to do |format|
      format.html
      format.json {render json: @plant}
    end
  end

  # Vista para editar.
  def edit
    @plant = Plant.find(params[:id])
    render "edit"
  end

  def update
    @plant = Plant.find(params[:id])
    #@plant.user_id = session[:user]

    if plant_params[:images]
      
      if plant_params[:images].length.to_i <= 5

      if @plant.images.length > 0
        for i in 0...@plant.images.length
          File.delete(Rails.public_path + "uploads/plants/#{i}_#{@plant.name}.#{@plant.images[0].split(".").last}")
        end
        @plant.images = []
      end

        for i in 0...plant_params[:images].length
          if plant_params[:images][i] && !plant_params[:images][i].is_a?(String)
            if @plant.images[i] && Dir.glob("public#{i}_#{@plant.name}").count > 0
              File.delete("public#{i}_#{@plant.name}")
            end

            @plant.images.push( "/uploads/plants/#{i}_#{@plant.name}.#{plant_params[:images][i].original_filename.split(".").last}" )
            
            File.open('public' + @plant.images[i], "wb") do |f|
              f.write(plant_params[:images][i].read)
            end

          end
          params[:plant][:images][i] = @plant.images[i]
        end

        if @plant.update(plant_params)
          redirect_to @plant
        else
          render "edit"
        end

      else
        @plant.errors.add(:images, "Maximo 5 imgs") if plant_params[:images].length.to_i > 5
        render "new_plant"
      end

    else
      if @plant.images.length > 0
        for i in 0...@plant.images.length
          File.delete(Rails.public_path + "uploads/plants/#{i}_#{@plant.id}.#{@plant.images[0].split(".").last}")
        end
        @plant.images = []
      end
      
      if @plant.update(plant_params)
        redirect_to @plant
      else
        render "edit"
      end  
    end
  end

  def addfavs
    @user = User.find(session[:user]) if session[:user]
    @plant = Plant.find(params[:id])
    puts "EL ID: " + @user.id.to_s


    if !@plant.favs.include?(@user.id)

      @plant.favs.push(@user.id)
      puts "ARR: " + @plant.favs.to_s
      
      if @plant.update(plant_params)
        redirect_to @plant
      end

    else
      @plant.favs.delete(@user.id)

      if @plant.update(plant_params)
        redirect_to @plant
      end
      #redirect_to @plant
    end

  end

  def destroy
    @plant = Plant.find(params[:id])
    @plant.destroy
    redirect_to plants_path
  end

  private
  def plant_params
    params.require(:plant).permit(:name, :common_name, :resume,:description,:distribution,:etnobotanic, :latitude, :longitude, :user_id, :favs, images:[] )
  end
end
