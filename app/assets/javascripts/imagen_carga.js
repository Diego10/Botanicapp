$(document).ready(function(){

	  $("#enviar").attr("disabled",true); 

	  $("#pass").keyup(function() {
	    var pass = $("#pass").val();
	    var pass_confirmation = $("#pass_c").val();
	    if ( pass != pass_confirmation ) { 
	      //console.log("Deberia deshabilitarlo");
	      $("#enviar").attr("disabled",true); 
	    }
	    else{
	      //console.log("Deberia habilitarlo");
	      $("#enviar").attr("disabled",false); 
	    }
	  });

	  $("#pass_c").keyup(function() {
	    var pass = $("#pass").val();
	    var pass_confirmation = $("#pass_c").val();
	    if ( pass != pass_confirmation ) { 
	      //console.log("Deberia deshabilitarlo");
	      $("#enviar").attr("disabled",true); 
	    }
	    else{
	      //console.log("Deberia habilitarlo");
	      $("#enviar").attr("disabled",false); 
	    }
	  });
	});

	