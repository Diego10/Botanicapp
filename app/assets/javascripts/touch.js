//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= requre bootstrap-sprockets
//= require-tree .
//= require bootstrap.min
//= require jquery.mobile.custom.js
//= require jquery.mobile.custom.min.js

jQuery(document).on("ready", function() {
	/*
	$("#myCarousel").touchwipe({
		wipeLeft: function(){$("#myCarousel").carousel('next')},
		wipeRight: function(){$("#myCarousel").carousel('prev')},
		min_move_x: 20,
		preventDefaultEvents: false
	});*/

  	$("#myCarousel").swiperight(function() {  
		  $(this).carousel('prev');  
	});  
    $("#myCarousel").swipeleft(function() {  
	      $(this).carousel('next');  
	});  

	$('.carousel').carousel({
  		interval: 0
	})

});