// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= requre bootstrap-sprockets
//= require-tree .
//= require bootstrap.min
//= require jquery.mobile.custom.js
//= require jquery.mobile.custom.min.js

    var nombreVista = window.location.pathname;
    //console.log(mapaVista);

    if(nombreVista != "/map" ){

        function initialize() {
          //document.getElementById("enviar").disabled = true;
        }

        function loadScript() {
          var script = document.createElement('script');
          script.type = 'text/javascript';
          script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
              '&signed_in=true&callback=initialize';
          document.body.appendChild(script);
        }

        window.onload = loadScript;
    }
/*
$(document).ready(function(){
    $('body').bind('touchmove', function(e){e.preventDefault()})

  });
  */


$(document).ready(function(){
    // Código para la vista previa de la 
  // imagen que va a subir un usuario.
  function showImage(image) {
    console.log(image.length);
    if (image.files && image.files[0]) {
    console.log(image.files[0]);
      var reader = new FileReader();
      reader.onload = function(e) {
          $('.thumbnail').attr('src', e.target.result);
        $('.thumbnail').width(200); 
      }
      reader.readAsDataURL(image.files[0]);
    }
  }

  $(".input_image").change(function() {
    showImage(this);
  });

  function showImageM(image,id,valor) {
    //console.log(image.length);
    if (image.files && image.files[valor]) {
    console.log(image.files[valor]);
      var reader = new FileReader();
      reader.onload = function(e) {
          $(id).attr('src', e.target.result);
        $(id).width(200); 
      }
      reader.readAsDataURL(image.files[valor]);
    }
  }

  $(".input_imageM").change(function() {

    $('.imagenes').html('');

    for(i=0;i<this.files.length;i++){
      var imgID = '#thumbnail_'+i
        var oImg=document.createElement("img");
        oImg.setAttribute('class','thumbnail');
        oImg.setAttribute('id','thumbnail_'+i);
        $('.imagenes').append(oImg);
      showImageM(this,imgID,i);
    }

  });
});